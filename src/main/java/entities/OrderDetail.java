package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class OrderDetail {
	private static final long serialVersionUID = 1L;
	
	private Long id_detail;
	private Book book;
	
	public OrderDetail() {
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_DETAIL", unique = true, nullable = false)
	public Long getId_detail() {
		return id_detail;
	}
	public void setId_detail(Long id_detail) {
		this.id_detail = id_detail;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	
}
