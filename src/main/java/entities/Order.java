package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table
public class Order {
	private static final long serialVersionUID = 1L;
	
	private Long id_order;
	private Book book;
	private Address address;
	private User user;
	private Date date_purchse;
	
	public Order() {
	}
	
	public Order(Date date_purchse) {
		this.date_purchse = date_purchse;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_ORDER", unique = true, nullable = false)
	public Long getId_order() {
		return id_order;
	}

	public void setId_order(Long id_order) {
		this.id_order = id_order;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "DATE_PURCHSE" , length = 50)
	public Date getDate_purchse() {
		return date_purchse;
	}

	public void setDate_purchse(Date date_purchse) {
		this.date_purchse = date_purchse;
	}

	
}
