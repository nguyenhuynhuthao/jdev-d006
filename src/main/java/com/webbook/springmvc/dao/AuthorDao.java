package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Author;

@Repository
public class AuthorDao extends Dao<Author> {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Author> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Author").list();
	}
	
	@Override
	public Author get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Author) session.get(Author.class, new Long(id));
	}

	@Override
	public Author add(Author author) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(author);
		return author;
	}

	@Override
	public Boolean update(Author author) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(author);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;				
		}
	}

	@Override
	public Boolean delete(Author author) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != author) {
			try {
				session.delete(author);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Author author = (Author) session.load(Author.class, new Long(id));
		if (null != author) {
			session.delete(author);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
