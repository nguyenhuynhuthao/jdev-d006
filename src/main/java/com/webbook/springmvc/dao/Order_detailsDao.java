package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Order_details;

@Repository
public class Order_detailsDao extends Dao<Order_details>{
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Order_details> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Order_details").list();
	}
	@Override
	public Order_details get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Order_details) session.get(Order_details.class, new Long(id));
	}
	@Override
	public Order_details add(Order_details orderdetail) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(orderdetail);
		return orderdetail;
	}
	@Override
	public Boolean update(Order_details orderdetail) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(orderdetail);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}
	
	@Override
	public Boolean delete(Order_details orderdetail) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != orderdetail) {
			try {
				session.delete(orderdetail);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}
	@Override
	public Boolean delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Order_details orderdetail = (Order_details) session.load(Order_details.class, new Long(id));
		if (null != orderdetail) {
			session.delete(orderdetail);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
