package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.webbook.springmvc.entities.Genre;

public class GenreDao extends Dao<Genre> {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Genre> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from Gence").list();
	}
	@Override
	public Genre get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (Genre) session.get(Genre.class, new Long(id));
	}
	@Override
	public Genre add(Genre genre) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(genre);
		return genre;
	}
	@Override
	public Boolean update(Genre genre) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(genre);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}
	
	@Override
	public Boolean delete(Genre genre) {
		Session session = this.sessionFactory.getCurrentSession();
		if (null != genre) {
			try {
				session.delete(genre);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}
	@Override
	public Boolean delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Genre genre = (Genre) session.load(Genre.class, new Long(id));
		if (null != genre) {
			session.delete(genre);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
}
