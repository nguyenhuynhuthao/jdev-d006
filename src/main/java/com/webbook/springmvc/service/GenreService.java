package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Genre;

@Transactional
@Service
public class GenreService {
	@Autowired
	Dao<Genre> genreDAO;
	
	public List<Genre> getAll(){
		return genreDAO.getAll();
	}
	
	public Genre get(Long id){
		return genreDAO.get(id);
	}
	
	public Genre add(Genre t){
		return genreDAO.add(t);
	}
	
	public Boolean update(Genre t){
		return genreDAO.update(t);
	}
	
	public Boolean delete(Genre t){
		return genreDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return genreDAO.delete(id);
	}
}
