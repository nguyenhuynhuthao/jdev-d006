package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Order_details;

@Transactional
@Service
public class Order_detailsService {
	@Autowired
	Dao<Order_details> orderdetailDAO;
	
	public List<Order_details> getAll(){
		return orderdetailDAO.getAll();
	}
	
	public Order_details get(Long id){
		return orderdetailDAO.get(id);
	}
	
	public Order_details add(Order_details t){
		return orderdetailDAO.add(t);
	}
	
	public Boolean update(Order_details t){
		return orderdetailDAO.update(t);
	}
	
	public Boolean delete(Order_details t){
		return orderdetailDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return orderdetailDAO.delete(id);
	}
}
