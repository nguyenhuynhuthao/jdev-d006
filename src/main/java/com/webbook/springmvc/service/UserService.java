package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.User;

@Transactional
@Service
public class UserService {
	
	@Autowired
	Dao<User> UserDAO;
	
	public List<User> getAll(){
		return UserDAO.getAll();
	}
	
	public User get(Long id){
		return UserDAO.get(id);
	}
	
	public User add(User t){
		return UserDAO.add(t);
	}
	
	public Boolean update(User t){
		return UserDAO.update(t);
	}
	
	public Boolean delete(User t){
		return UserDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return UserDAO.delete(id);
	}
}
