package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "book", catalog = "webbook")
public class Book implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id_book;
	private String name_book;
	private Long price;
	private Set<Author> author = new HashSet<Author>();
	private Set<Genre> genre = new HashSet<Genre>();
	private List<Publisher> publisher = new ArrayList<Publisher>();
	
	public Book(){
		
	}
	
	public Book(String name_book, Long price){
		this.name_book = name_book;
		this.price = price;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_BOOK", unique = true, nullable = false)
	public Long getId_book() {
		return id_book;
	}

	public void setId_book(Long id_book) {
		this.id_book = id_book;
	}
	
	@Column(name = "Name_Book" , length = 50)
	public String getName_book() {
		return name_book;
	}

	public void setName_book(String name_book) {
		this.name_book = name_book;
	}

	@Column(name = "Price" , length = 50)
	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public Set<Author> getAuthor() {
		return author;
	}

	public void setAuthor(Set<Author> author) {
		this.author = author;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public Set<Genre> getGenre() {
		return genre;
	}

	public void setGenre(Set<Genre> genre) {
		this.genre = genre;
	}
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinTable(name = "BOOK_PUBLISHER", joinColumns = { @JoinColumn(name = "ID_BOOK") }, inverseJoinColumns = {
			@JoinColumn(name = "ID_PUBLISHER") })
	public List<Publisher> getPublisher() {
		return publisher;
	}

	public void setPublisher(List<Publisher> publisher) {
		this.publisher = publisher;
	}


}
