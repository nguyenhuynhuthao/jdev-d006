package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "genre" , catalog = "webbook")
public class Genre {
	private static final long serialVersionUID = 1L;
	
	private Long id_genre;
	private String description;
	
	public Genre() {
	}
	
	public Genre(String description) {
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_GENRE", unique = true, nullable = false)
	public Long getId_gence() {
		return id_genre;
	}

	public void setId_gence(Long id_gence) {
		this.id_genre = id_genre;
	}

	@Column(name = "DESCRIPTION" , length = 100)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
