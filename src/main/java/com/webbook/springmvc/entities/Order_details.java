package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "order_details", catalog = "webbook")
public class Order_details {
	private static final long serialVersionUID = 1L;
	
	private Long id_detail;
	private String message;
	private Long total_price;
	private Address address;
	private Set<Book> book = new HashSet<Book>();
	
	public Order_details() {
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_DETAIL", unique = true, nullable = false)
	public Long getId_detail() {
		return id_detail;
	}
	public void setId_detail(Long id_detail) {
		this.id_detail = id_detail;
	}

	@Column(name = "MESSAGE" , length = 100)
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "TOTAL_PRICE" , length = 50)
	public Long getTotal_price() {
		return total_price;
	}

	public void setTotal_price(Long total_price) {
		this.total_price = total_price;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "order_details")
	public Set<Book> getBook() {
		return book;
	}

	public void setBook(Set<Book> book) {
		this.book = book;
	}
	
}

