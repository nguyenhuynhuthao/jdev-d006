package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Book;

@Transactional
@Service
public class BookService {
	
	@Autowired
	Dao<Book> addressDAO;
	
	public List<Book> getAll(){
		return addressDAO.getAll();
	}
	
	public Book get(Long id){
		return addressDAO.get(id);
	}
	
	public Book add(Book t){
		return addressDAO.add(t);
	}
	
	public Boolean update(Book t){
		return addressDAO.update(t);
	}
	
	public Boolean delete(Book t){
		return addressDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return addressDAO.delete(id);
	}
}