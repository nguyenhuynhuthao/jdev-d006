package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.OrderDetail;

@Transactional
@Service
public class OrderDetailService {
	
	@Autowired
	Dao<OrderDetail> orderdetailDAO;
	
	public List<OrderDetail> getAll(){
		return orderdetailDAO.getAll();
	}
	
	public OrderDetail get(Long id){
		return orderdetailDAO.get(id);
	}
	
	public OrderDetail add(OrderDetail t){
		return orderdetailDAO.add(t);
	}
	
	public Boolean update(OrderDetail t){
		return orderdetailDAO.update(t);
	}
	
	public Boolean delete(OrderDetail t){
		return orderdetailDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return orderdetailDAO.delete(id);
	}
}