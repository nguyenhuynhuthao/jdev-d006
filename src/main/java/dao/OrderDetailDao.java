package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.OrderDetail;

@Repository
public class OrderDetailDao extends Dao<OrderDetail>{

		@Autowired
		private SessionFactory sessionFactory;
		
		@Override
		public List<OrderDetail> getAll() {
			Session session = this.sessionFactory.getCurrentSession();
			return session.createQuery("from OrderDetail").list();
		}
		@Override
		public OrderDetail get(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			return (OrderDetail) session.get(OrderDetail.class, new Long(id));
		}
		@Override
		public OrderDetail add(OrderDetail orderdetail) {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(orderdetail);
			return orderdetail;
		}
		@Override
		public Boolean update(OrderDetail orderdetail) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				session.update(orderdetail);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		
		@Override
		public Boolean delete(OrderDetail orderdetail) {
			Session session = this.sessionFactory.getCurrentSession();
			if (null != orderdetail) {
				try {
					session.delete(orderdetail);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
			return Boolean.FALSE;
		}
		@Override
		public Boolean delete(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			OrderDetail orderdetail = (OrderDetail) session.load(OrderDetail.class, new Long(id));
			if (null != orderdetail) {
				session.delete(orderdetail);
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
}
