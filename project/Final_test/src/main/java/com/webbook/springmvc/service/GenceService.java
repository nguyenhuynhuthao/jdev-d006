package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Gence;

@Transactional
@Service
public class GenceService {
	
	@Autowired
	Dao<Gence> genceDAO;
	
	public List<Gence> getAll(){
		return genceDAO.getAll();
	}
	
	public Gence get(Long id){
		return genceDAO.get(id);
	}
	
	public Gence add(Gence t){
		return genceDAO.add(t);
	}
	
	public Boolean update(Gence t){
		return genceDAO.update(t);
	}
	
	public Boolean delete(Gence t){
		return genceDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return genceDAO.delete(id);
	}
}