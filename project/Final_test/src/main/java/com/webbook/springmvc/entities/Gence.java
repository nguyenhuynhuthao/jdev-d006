package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Gence {
	private static final long serialVersionUID = 1L;
	
	private Long id_gence;
	private String description;
	
	public Gence() {
	}
	
	public Gence(String description) {
		this.description = description;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_GENCE", unique = true, nullable = false)
	public Long getId_gence() {
		return id_gence;
	}

	public void setId_gence(Long id_gence) {
		this.id_gence = id_gence;
	}

	@Column(name = "DESCRIPTION" , length = 100)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
