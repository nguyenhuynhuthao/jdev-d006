package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Publisher;

@Transactional
@Service
public class PublisherService {
	
	@Autowired
	Dao<Publisher> publisherDAO;
	
	public List<Publisher> getAll(){
		return publisherDAO.getAll();
	}
	
	public Publisher get(Long id){
		return publisherDAO.get(id);
	}
	
	public Publisher add(Publisher t){
		return publisherDAO.add(t);
	}
	
	public Boolean update(Publisher t){
		return publisherDAO.update(t);
	}
	
	public Boolean delete(Publisher t){
		return publisherDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return publisherDAO.delete(id);
	}
}