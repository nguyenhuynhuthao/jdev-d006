package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Order;

@Transactional
@Service
public class OrderService {
	
	@Autowired
	Dao<Order> orderDAO;
	
	public List<Order> getAll(){
		return orderDAO.getAll();
	}
	
	public Order get(Long id){
		return orderDAO.get(id);
	}
	
	public Order add(Order t){
		return orderDAO.add(t);
	}
	
	public Boolean update(Order t){
		return orderDAO.update(t);
	}
	
	public Boolean delete(Order t){
		return orderDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return orderDAO.delete(id);
	}
}