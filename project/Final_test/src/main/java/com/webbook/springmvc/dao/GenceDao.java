package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.Gence;

@Repository
public class GenceDao extends Dao<Gence>{

		@Autowired
		private SessionFactory sessionFactory;
		
		@Override
		public List<Gence> getAll() {
			Session session = this.sessionFactory.getCurrentSession();
			return session.createQuery("from Gence").list();
		}
		@Override
		public Gence get(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			return (Gence) session.get(Gence.class, new Long(id));
		}
		@Override
		public Gence add(Gence gence) {
			Session session = this.sessionFactory.getCurrentSession();
			session.save(gence);
			return gence;
		}
		@Override
		public Boolean update(Gence gence) {
			Session session = this.sessionFactory.getCurrentSession();
			try {
				session.update(gence);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		
		@Override
		public Boolean delete(Gence gence) {
			Session session = this.sessionFactory.getCurrentSession();
			if (null != gence) {
				try {
					session.delete(gence);
					return Boolean.TRUE;
				} catch (Exception e) {
					return Boolean.FALSE;
				}
			}
			return Boolean.FALSE;
		}
		@Override
		public Boolean delete(Long id) {
			Session session = this.sessionFactory.getCurrentSession();
			Gence gence = (Gence) session.load(Gence.class, new Long(id));
			if (null != gence) {
				session.delete(gence);
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
}
