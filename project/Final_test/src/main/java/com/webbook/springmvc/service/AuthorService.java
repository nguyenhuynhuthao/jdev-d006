package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;

@Transactional
@Service
public class AuthorService<Author> {
	
	@Autowired
	Dao<Author> AuthorDAO;
	
	public List<Author> getAll(){
		return AuthorDAO.getAll();
	}
	
	public Author get(Long id){
		return AuthorDAO.get(id);
	}
	
	public Author add(Author t){
		return AuthorDAO.add(t);
	}
	
	public Boolean update(Author t){
		return AuthorDAO.update(t);
	}
	
	public Boolean delete(Author t){
		return AuthorDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return AuthorDAO.delete(id);
	}
}