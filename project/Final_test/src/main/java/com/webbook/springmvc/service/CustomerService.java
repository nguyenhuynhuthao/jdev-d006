package com.webbook.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.webbook.springmvc.dao.Dao;
import com.webbook.springmvc.entities.Customer;

@Transactional
@Service
public class CustomerService {
	
	@Autowired
	Dao<Customer> customerDAO;
	
	public List<Customer> getAll(){
		return customerDAO.getAll();
	}
	
	public Customer get(Long id){
		return customerDAO.get(id);
	}
	
	public Customer add(Customer t){
		return customerDAO.add(t);
	}
	
	public Boolean update(Customer t){
		return customerDAO.update(t);
	}
	
	public Boolean delete(Customer t){
		return customerDAO.delete(t);
	}
	
	public Boolean delete(Long id){
		return customerDAO.delete(id);
	}
}