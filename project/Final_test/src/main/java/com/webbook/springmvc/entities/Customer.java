package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "customer" , catalog = "webbook")
public class Customer {
	private static final long serialVersionUID = 1L;
	
	private Long id_customer;
	private String phone_customer;
	private String name_customer;
	private String message;
	
	public Customer() {
	}
	
	public Customer(String phone_customer, String name_customer, String message) {
		this.phone_customer = phone_customer;
		this.name_customer = name_customer;
		this.message = message;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_CUSTOMER", unique = true, nullable = false)
	public Long getId_customer() {
		return id_customer;
	}

	public void setId_customer(Long id_customer) {
		this.id_customer = id_customer;
	}

	@Column(name = "PHONE_CUSTOMER" , length = 50)
	public String getPhone_customer() {
		return phone_customer;
	}

	public void setPhone_customer(String phone_customer) {
		this.phone_customer = phone_customer;
	}

	@Column(name = "NAME_CUSTOMER" , length = 50)
	public String getName_customer() {
		return name_customer;
	}

	public void setName_customer(String name_customer) {
		this.name_customer = name_customer;
	}

	@Column(name = "MESSAGE" , length = 100)
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
