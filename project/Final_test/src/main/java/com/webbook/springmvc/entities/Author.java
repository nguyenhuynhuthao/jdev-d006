package com.webbook.springmvc.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "author" , catalog = "webbook")
public class Author {
	private static final long serialVersionUID = 1L;
	
	private Long id_author;
	private String name_author;
	private Address address;
	
	public Author(){
	}
	
	public Author(String name_author){
		this.name_author = name_author;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_AUTHOR", unique = true, nullable = false)
	public Long getId_author() {
		return id_author;
	}

	public void setId_author(Long id_author) {
		this.id_author = id_author;
	}

	@Column(name = "NAME_AUTHOR" , length = 50)
	public String getName_author() {
		return name_author;
	}

	public void setName_author(String name_author) {
		this.name_author = name_author;
	}

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
}
