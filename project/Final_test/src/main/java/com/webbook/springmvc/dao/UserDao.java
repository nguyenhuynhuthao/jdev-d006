package com.webbook.springmvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.webbook.springmvc.entities.User;

@Repository
public class UserDao extends Dao<User> {
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<User> getAll() {
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("from User").list();
	}
	
	@Override
	public User get(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		return (User) session.get(User.class, new Long(id));
	}

	@Override
	public User add(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(user);
		return user;
	}

	@Override
	public Boolean update(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.update(user);
			return Boolean.TRUE;
		} catch (Exception e) {
			return Boolean.FALSE;
		}
	}

	@Override
	public Boolean delete(User user) {
		Session session = this.sessionFactory.getCurrentSession();
		if(null != user) {
			try {
				session.delete(user);
				return Boolean.TRUE;
			} catch (Exception e) {
				return Boolean.FALSE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public Boolean delete(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		User user = (User) session.load(User.class, new Long(id));
		if (null != user) {
			session.delete(user);
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
}
